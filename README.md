# README #

Source code is developed for testing the performance of http://useclark.com/demo page using UIWebView and WKWebView.

# UseClark Performance Optimisation #

## What is current issue? ##
With testing of demo page(http://useclark.com/demo) on iPad mini(iOS8), found the following issues.

####Good performance functions:####
* Tapping xray button, read button, summary button, help button.
* Auto positioning of add-sentence-button.
* The animation of moving current sentence is smooth. 

####Bad performance functions:####
* Loading is slow.
* Scrolling feels heavy.
* Scrolling does not response sometimes.
* Speed slider bar is not smooth.


## Solution ##

### 1. Loading speed ###
Loading speed is a bit slow, but acceptable.  
Google page speed insights showed some clues on how to improve loading speed.  
https://developers.google.com/speed/pagespeed/insights/?url=useclark.com%2Fdemo  
Users use UseClark starting from parsing pdf or other format text instead of loading a web page, I skipped this issue. 

### 2. Scrolling feels heavy ###
#### Profiling ####
To understand what's going on, I used Instruments(a performance tool) to check frame per second of web view and used Safari web inspector to check where is time spent.

As be shown in the following picture, fps is surprisingly almost always 60, although scrolling feels heavy.  

![Screen Shot 2015-10-09 at 12.09.30 AM copy.png](https://bitbucket.org/repo/AAp4qz/images/884376383-Screen%20Shot%202015-10-09%20at%2012.09.30%20AM%20copy.png)  

Result in Safari web inspector shows that most of running time is spent on running javascript.

![Screen Shot 2015-10-09 at 12.25.24 AM.png](https://bitbucket.org/repo/AAp4qz/images/2229729299-Screen%20Shot%202015-10-09%20at%2012.25.24%20AM.png)

#### Assumption and Solution ####
This issue seems not related to javascript, because demo page works fine in safari browser.
So I thought it must have something to do with the web view component itself.   
After some digging, I found that decelerationRate of scrollview of UIWebView is causing the problem.   
By adding following code `webView.scrollView.decelerationRate = 2;` to change UIScrollView's deceleration rate perfectly solve the issue. 


### 3. Scrolling does not response sometimes ###
#### What is the scenario? ####
Web view does not response to scroll gesture sometimes, especially when scrolling stopped and then I immediately start scroll web view again.  

#### First Assumption ####
At first, I thought this issue is related to UIWebView, because UIWebView will pause all JavaScript execution whenever the user scrolled.  
In iOS8, WKWebView improved. WKWebView won't pause all JavaScript execution when user scrolled.
Besides WKWebView has a big performance improve, so I implemented the WKWebView(included in source code) and tested it with demo page.  
The result turned out that using WKWebView could not fix this issue.   
Meanwhile, this issue also happens in Safari browser.  

#### Second Assumption ####
So I think this issue is related to javascript.  
My guess is that javascript disabled scrolling when automatically reposition the add-sentence-button and current-sentence.  
I found the following code might be the cause. (Although I'm not sure.)   
If this is the cause, changing this logic would be able to solve this problem.  
```
$('body')
.on('click', 'a', preventDefault)
.on('tap', stopIfReading.bind(null, $, readingApi))
.on('touchmove', function (ev) {
  if (!$('html').hasClass('uc-reading-enabled')) return;
  ev.preventDefault();
})
...
var moveToSentence = function (api, $, sentencePosition) {
  if ($('html').hasClass('uc-reading-enabled')) return;
  var $newSentence = $('.uc-current-sentence').data('uc-' + sentencePosition).valueOrElse($());
  var $newSentenceWord = getSentenceFirstWord($newSentence);
  if ($newSentenceWord.length) {
    api.setCurrentPosition($newSentenceWord, {
      scroll: true
    });
  }
};
```

### 4. Speed slider bar is not smooth ###

```
var ReadingMenu = ui.component('ReadingMenu', {
  ...
  onSliderChange: function (newValue) {
    this.props.reading.setSpeed(newValue);
  },
  ...
})
```
By reading code, my guess is that `setSpeed` in javascript code is constantly called when sliding, which makes the scroll not smooth.  
Run `setSpeed` asynchronously or run `setSpeed` only sliding stops might be able to fix this problem.  


## Reference ##
* [Why the Scroll Event Change in iOS 8 is a Big Deal](http://developer.telerik.com/featured/scroll-event-change-ios-8-big-deal/)
* [A performance test on 4 web view: UIWebView, WKWebView, SFSafariViewController, Safari](http://grandbig.github.io/blog/2015/09/21/ios9-webview/)