//
//  AppDelegate.h
//  UseClarkIpad
//
//  Created by Ian on 10/4/15.
//  Copyright (c) 2015 Ian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *fileRelativePath;
@end

