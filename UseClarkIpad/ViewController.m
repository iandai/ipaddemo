//
//  ViewController.m
//  UseClarkIpad
//
//  Created by Ian on 10/4/15.
//  Copyright (c) 2015 Ian. All rights reserved.
//

#import "ViewController.h"
#import "TET_ios/TET_objc.h"
#import "AppDelegate.h"


@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic) NSString *pdfText;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSString* url = @"http://useclark.com/demo";
    NSURL* nsUrl = [NSURL URLWithString:url];
    NSURLRequest* request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30];
    [self.webView loadRequest:request];
    self.webView.scrollView.decelerationRate = 2;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end





// TODO: move it to another view controller

//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];

//- (void)didBecomeActive {
//
//    // extract text
//    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    NSString *infile = appDelegate.fileRelativePath;
//    if (infile) {
//        [self extractText:infile];
//    }
//
//    // replace demo text
//    if ([self.pdfText length]) {
//
//        NSArray *rows = [self.pdfText componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
//
//        NSMutableString *rowsHTML = [[NSMutableString alloc] init];
//        for (NSString *line in rows) {
//            NSMutableString *lineHTML = [[NSMutableString alloc] init];
//            [lineHTML appendString:@"<p><uc-span><uc-sentence>"];
//            NSArray *words = [line componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//            for (NSString *word in words) {
//                NSString *wordHTML = [NSString stringWithFormat:@"<uc-word>%@</uc-word>", word];
//                [lineHTML appendString:wordHTML];
//            }
//            [lineHTML appendString:@"</uc-sentence></uc-span></p>"];
//            [rowsHTML appendString:lineHTML];
//        }
//
//        NSMutableString *sectionHTML = [[NSMutableString alloc] init];
//        [sectionHTML appendString:@"<section><h1>PDF Text</h1>"];
//        [sectionHTML appendString:rowsHTML];
//        [sectionHTML appendString:@"</section>"];
//
//        NSString *contentHTML = [NSString stringWithFormat: @"$('.demo').prepend('%@')", sectionHTML];
//        [self.webView stringByEvaluatingJavaScriptFromString:contentHTML];
//
//    }
//}
//
//- (void) extractText:(NSString *)infile {
//
//    // Find our documents directory
//    NSArray *dirPaths;
//    NSString *documentsDir;
//    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    documentsDir = [dirPaths objectAtIndex:0];
//
//
//    /* global option list */
//    NSString *globaloptlist =
//    [NSString stringWithFormat:@"searchpath={{%@} {%@/extractor_ios.app} {%@/extractor_ios.app/resource/cmap}}",
//     documentsDir, NSHomeDirectory(), NSHomeDirectory()];
//
//    /* option list to switch on TET logging */
//    //NSString *loggingoptlist = [NSString stringWithFormat:@"logging {filename={%@/trace.txt} remove}", documentsDir];
//
//    /* document-specific option list */
//    NSString *docoptlist = @"";
//
//    /* page-specific option list */
//    NSString *pageoptlist = @"granularity=page";
//
//    /* separator to emit after each chunk of text. This depends on the
//     * application's needs;
//     * for granularity=word a space character may be useful.
//     */
//#define SEPARATOR @"\n"
//
//    int pageno = 0;
//    NSMutableString *pdfText = [[NSMutableString alloc]init];;
//    NSString *warningText=@"";
//
//    TET *tet = [[TET alloc] init];
//    if (!tet) {
//        return;
//    }
//
//    @try {
//        NSInteger n_pages;
//        NSInteger doc;
//
//        //[tet set_option:loggingoptlist];
//        [tet set_option:globaloptlist];
//
//        doc = [tet open_document:infile optlist:docoptlist];
//
//        if (doc == -1)
//        {
//            warningText = [NSString stringWithFormat:@"Error %ld in %@(): %@\n",
//                           (long)[tet get_errnum], [tet get_apiname], [tet get_errmsg]];
//            [self displayError:warningText];
//            return;
//        }
//
//        /* get number of pages in the document */
//        n_pages = (NSInteger) [tet pcos_get_number:doc path:@"length:pages"];
//
//        /* loop over pages in the document */
//        for (pageno = 1; pageno <= n_pages; ++pageno)
//        {
//            NSString *text;
//            NSInteger page;
//
//            page = [tet open_page:doc pagenumber:pageno optlist:pageoptlist];
//
//            if (page == -1)
//            {
//                warningText = [NSString stringWithFormat:@"%@\nError %ld in %@() on page %d: %@\n",
//                               warningText, (long)[tet get_errnum], [tet get_apiname], pageno, [tet get_errmsg]];
//                continue;                        /* try next page */
//            }
//
//            /* Retrieve all text fragments; This is actually not required
//             * for granularity=page, but must be used for other granularities.
//             */
//            while ((text = [tet get_text:page]) != nil)
//            {
//                [pdfText appendString:text];
//                [pdfText appendString:SEPARATOR];
//            }
//
//            if ([tet get_errnum] != 0)
//            {
//                warningText  = [NSString stringWithFormat:@"%@\nError %ld in %@() on page %d: %@\n", warningText, (long)[tet get_errnum], [tet get_apiname], pageno, [tet get_errmsg]];
//            }
//
//            [tet close_page:page];
//        }
//
//        [tet close_document:doc];
//    }
//
//    @catch (TETException *ex) {
//        NSString *exception=@"";
//        if (pageno == 1) {
//            exception = [NSString stringWithFormat:@"Error %ld in %@(): %@\n",
//                         (long)[ex get_errnum], [ex get_apiname], [ex get_errmsg]];
//        } else {
//            exception = [NSString stringWithFormat:@"Error %ld in %@() on page %d: %@\n",
//                         (long)[ex get_errnum], [ex get_apiname], pageno, [ex get_errmsg]];
//        }
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"TET crashed" message:exception
//                                                           delegate:self cancelButtonTitle:nil otherButtonTitles:@"ok", nil];
//        [alertView show];
//    }
//    @catch (NSException *ex) {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[ex name] message:[ex reason]
//                                                           delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//        [alertView show];
//    }
//    @finally {
//        if (tet)
//            tet = nil;
//    }
//
//
//    /* show the warning(s) that occured while processing the file */
//    if (warningText.length>0) {
//        [self displayError:warningText];
//    }
//
//    NSLog(@"%@", pdfText);
//    self.pdfText=pdfText;
//}
//
//- (void) displayError: (NSString *) message {
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"TET error" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"ok", nil];
//    [alertView show];
//}