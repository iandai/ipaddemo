//
//  WKWebViewController.swift
//  UseClarkIpad
//
//  Created by Ian on 10/7/15.
//  Copyright (c) 2015 Ian. All rights reserved.
//

import UIKit
import WebKit


class WKWebViewController: UIViewController, WKNavigationDelegate {
    
    var webView: WKWebView
    var startTime: CFAbsoluteTime!
    
    required init(coder aDecoder: NSCoder) {
        self.webView = WKWebView(frame: CGRectZero)
        super.init(coder: aDecoder)
        self.webView.navigationDelegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(webView)
        
        webView.setTranslatesAutoresizingMaskIntoConstraints(false)
        let height = NSLayoutConstraint(item: webView, attribute: .Height, relatedBy: .Equal, toItem: view, attribute: .Height, multiplier: 1, constant: -50)
        let width = NSLayoutConstraint(item: webView, attribute: .Width, relatedBy: .Equal, toItem: view, attribute: .Width, multiplier: 1, constant: 0)
        let top = NSLayoutConstraint(item: webView, attribute: .Top, relatedBy: .Equal, toItem: view, attribute: .Top, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: webView, attribute: .Bottom, relatedBy: .Equal, toItem: view, attribute: .Bottom, multiplier: 1, constant: -50)
        view.addConstraints([height, width, top, bottom])
        
        let url = NSURL(string:"http://useclark.com/demo")
        let request = NSURLRequest(URL:url!)
        webView.loadRequest(request)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


// TODO : delete
//        startTime = CFAbsoluteTimeGetCurrent()
//    func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
//
//        let elapsedTime = CFAbsoluteTimeGetCurrent() - startTime
//        let alert = UIAlertController(title: "Time ", message: "\(elapsedTime)", preferredStyle: .Alert)
//        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
//        presentViewController(alert, animated: true, completion: nil)
//    }